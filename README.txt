
-- SUMMARY --

Organic groups People Form module is for use cases when certain 
role have access to administrating users and should
see the options in "UPDATE OPTIONS" to add or remove
selected users to or from selected group. This module
provides this feature to easily and fast add/remove user
to/from group without the hassle.

-- REQUIREMENTS --

Organic groups 2.x

-- INSTALLATION --

	* Enable Organic groups People Form module;
	* In admin/people/permissions add permission called 
		"Access Organic groups People Form module" to the roles which 
		will be able to use this module;
	* In admin/people/permissions add permission called
		"Administer users" to the same roles You added
		"Allow adding people to groups through People Form module"
		permission.

-- CONTACT --

http://drupal.org/user/1854866
